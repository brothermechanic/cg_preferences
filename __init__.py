'''
Copyright (C) 2023 brothermechanic@gmail.com

Created by brothermechanic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from bpy.app.handlers import persistent
import addon_utils
import time
import _thread
import bpy
bl_info = {
    "name": "CG Preferences",
    "author": "brothermechanic",
    "version": (0, 1),
    "blender": (3, 1, 0),
    "location": "",
    "description": "CG Preferences",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "System",
}


@persistent
def cg_prefs(dummy):
    '''Recommended CG settings for blender'''
    addon_utils.enable('mesh_f2', default_set=True)
    addon_utils.enable('mesh_tools', default_set=True)
    addon_utils.enable('mesh_looptools', default_set=True)
    addon_utils.enable('mesh_tiny_cad', default_set=True)
    addon_utils.enable('node_wrangler', default_set=True)
    addon_utils.enable('space_view3d_modifier_tools', default_set=True)
    addon_utils.enable('io_import_images_as_planes', default_set=True)

    prefs = bpy.context.preferences

    prefs.addons['mesh_f2'].preferences.autograb = False
    prefs.addons['cycles'].preferences.compute_device_type = 'OPTIX'

    prefs.use_preferences_save = False
    prefs.view.show_navigate_ui = False
    prefs.view.render_display_type = 'AREA'
    prefs.view.filebrowser_display_type = 'SCREEN'
    prefs.view.show_statusbar_stats = True
    prefs.view.show_statusbar_memory = True
    prefs.view.show_statusbar_vram = True
    prefs.edit.keyframe_new_handle_type = 'VECTOR'
    prefs.inputs.use_drag_immediately = False
    prefs.inputs.view_rotate_method = 'TRACKBALL'
    prefs.edit.undo_steps = 64
    prefs.system.audio_device = 'None'
    prefs.view.use_save_prompt = False
    prefs.filepaths.file_preview_type = 'SCREENSHOT'
    prefs.filepaths.use_file_compression = True

    prefs.keymap.active_keyconfig = 'Blender'
    keymapa = bpy.context.window_manager.keyconfigs.addon

    keymapa.keymaps.new(name='Object Mode', space_type='EMPTY').keymap_items.new(
        'object.subdivision_set', 'SIX', 'PRESS', ctrl=True).properties.level = 6
    keymapa.keymaps.new(name='Object Mode', space_type='EMPTY').keymap_items.new(
        'object.origin_clear', 'O', 'PRESS', alt=True)
    keymapa.keymaps.new(name='Object Mode', space_type='EMPTY').keymap_items.new(
        'object.convert', 'C', 'PRESS', alt=True)
    keymapa.keymaps.new(name='Object Mode', space_type='EMPTY').keymap_items.new(
        'object.make_single_user', 'U', 'PRESS').properties.obdata = True
    keymapa.keymaps.new(name='Mesh', space_type='EMPTY').keymap_items.new(
        'mesh.poke', 'P', 'PRESS', alt=True)
    keymapa.keymaps.new(name='Mesh', space_type='EMPTY').keymap_items.new(
        'mesh.select_non_manifold', 'M', 'PRESS', shift=True, ctrl=True, alt=True)
    keymapa.keymaps.new(name='Mesh', space_type='EMPTY').keymap_items.new(
        'object.subdivision_set', 'ZERO', 'PRESS', ctrl=True).properties.level = 0
    keymapa.keymaps.new(name='Mesh', space_type='EMPTY').keymap_items.new(
        'object.subdivision_set', 'TWO', 'PRESS', ctrl=True).properties.level = 2
    keymapa.keymaps.new(name='Console', space_type='EMPTY').keymap_items.new(
        'console.autocomplete', 'TAB', 'PRESS').repeat = True
    keymapa.keymaps.new(name='Window', space_type='EMPTY').keymap_items.new(
        'wm.window_fullscreen_toggle', 'F11', 'PRESS', oskey=True)
    keymapa.keymaps.new(name='Window', space_type='EMPTY').keymap_items.new(
        'wm.context_menu_enum', 'ACCENT_GRAVE', 'PRESS', oskey=True).properties.data_path = 'object.display_type'
    keymapa.keymaps.new(name='Screen', space_type='EMPTY').keymap_items.new(
        'ed.undo_history', 'Z', 'PRESS', ctrl=True, alt=True)

    print('CG Preferences activated!')


def delay_func():
    '''delayed function runing'''
    time.sleep(.5)
    keymap = bpy.context.window_manager.keyconfigs.active
    keymap.preferences.select_mouse = 'RIGHT'
    keymap.preferences.spacebar_action = 'TOOL'
    keymap.preferences.use_file_single_click = True
    keymap.keymaps['Window'].keymap_items['wm.quit_blender'].active = False


_thread.start_new_thread(delay_func, ())


def register():
    '''register blender module'''
    bpy.app.handlers.load_post.append(cg_prefs)


def unregister():
    '''unregister blender module'''
    bpy.app.handlers.load_post.remove(cg_prefs)


if __name__ == '__main__':
    register()

# CG Preferences
Python addon that lets you simply setup preferences and hotkeys.
Useful for new users and old users who like 2.7x like hotkeys

This addon is only compatible with Blender 3.X.

Features:
 * right click select
 * 2.7 like hotkeys
 * saving time for blender updating
 * possible to modify for your needs

Аддон настроек и горячих клавиш.
Будет удобно как для начинающих пользователей,
так и для профессионалов, кому нравились горячие клавиши Blender-2.7x.

Совместимо только с Blender-3.X.

Особенности:
 * выбор правой клавишей мыши
 * 2.7 похожие горячие клавиши
 * не нужно перенастраивать блендер при обновлении
 * возможность модификации настроек под ваши требования
